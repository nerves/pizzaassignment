﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Acr.UserDialogs;
using PizzaExercise.Models;

namespace PizzaExercise.ViewModels
{
    public class MainPageViewModel : INotifyPropertyChanged
    {
        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion


        private List<PizzaConfigurations> _configurations { get; set; }

        private ObservableCollection<string> _items;
        public ObservableCollection<string> Items
        {
            get
            {
                return _items;
            }

            set
            {
                if (value != _items)
                {
                    _items = value;
                    NotifyPropertyChanged();
                }
            }
        }

        public MainPageViewModel()
        {
            _configurations = new List<PizzaConfigurations>();
            _items = new ObservableCollection<string>();
        }

        public void Start()
        {
            Task.Run(async () =>
            {
                UserDialogs.Instance.ShowLoading();

                var (errorDetails, toppingsResults) = await WebService.GetPizzaActivity();
                if (errorDetails.Code == Utility.Enums.ErrorCode.Success)
                {
                    foreach (var toppingsResult in toppingsResults)
                    {
                        var config = new PizzaConfigurations();
                        config.Toppings.AddRange(toppingsResult.Toppings);
                        config.GenerateHash();
                        _configurations.Add(config);
                    }

                    var groupedList = _configurations.GroupBy(x => x.ConfigHash);
                    var sorted = groupedList.Select(grp => grp.ToList()).OrderByDescending(x => x.Count);

                    var rank = 1;
                    foreach (var group in sorted.Take(20))
                    {
                        Items.Add($"#{rank} - Ordered {group.Count} times \n\nToppings:\n{group[0].NiceToppingsString }");
                        rank++;
                    }

                    UserDialogs.Instance.HideLoading();
                }
                else
                {
                    UserDialogs.Instance.HideLoading();
                    UserDialogs.Instance.Alert("There was an error loading the data.", "Oops!");
                }
            });
        }
    }
}
