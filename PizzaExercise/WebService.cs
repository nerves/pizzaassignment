﻿
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using PizzaExercise.Models;
using static PizzaExercise.Utility.Enums;

namespace PizzaExercise
{
    public static class WebService
    {
        private static string ApUri = "http://files.olo.com/pizzas.json";

        public static async Task<(ErrorModel errorDetails, List<ToppingsResults> toppingsResults)> GetPizzaActivity()
        {
            ErrorModel errDetails = new ErrorModel { Code = ErrorCode.Fail, Description = "unknown error" }; ;

            using (var client = new HttpClient())
            {
                client.Timeout = TimeSpan.FromSeconds(30);
                client.BaseAddress = new Uri(ApUri);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                try
                {
                    HttpResponseMessage responseMessage = await client.GetAsync(ApUri).ConfigureAwait(false);

                    if (responseMessage.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        var response = await responseMessage.Content.ReadAsStringAsync();
                        var results = ToppingsResults.FromJson(response);
                        errDetails.Code = ErrorCode.Success;
                        return (errorDetails: errDetails, toppingsResults: results);
                    }
                    else
                    {
                        errDetails.Description = $"Error retreiving data : {responseMessage.StatusCode}";
                        return (errorDetails: errDetails, toppingsResults: new List<ToppingsResults>());
                    }
                }
                catch (Exception ex)
                {
                    errDetails.Description = ex.Message;
                    return (errorDetails: errDetails, toppingsResults: new List<ToppingsResults>());
                }
            }
        }
    }
}
