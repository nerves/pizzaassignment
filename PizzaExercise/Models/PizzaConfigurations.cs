﻿
using System.Collections.Generic;
using System.Linq;

namespace PizzaExercise.Models
{
    public class PizzaConfigurations
    {
        public List<Topping> Toppings { get; set; }
        public string ConfigHash { get; private set; }
        public string NiceToppingsString { get; private set; }
        public int ToppingsCount { get { return Toppings.Count; } }

        public PizzaConfigurations()
        {
            Toppings = new List<Topping>();
        }

        public void GenerateHash()
        {
            ConfigHash = string.Empty;
            foreach (var topping in Toppings.OrderByDescending(x => x))
            {
                ConfigHash += topping.ToString().ToLower();
                NiceToppingsString += string.IsNullOrEmpty(NiceToppingsString) ? $"{topping}" : $", {topping}";
            }
        }

    }
}
