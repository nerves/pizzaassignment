﻿using System;
using static PizzaExercise.Utility.Enums;

namespace PizzaExercise.Models
{

    public class ErrorModel
    {
        public ErrorCode Code { get; set; }
        public string Description { get; set; }

        public ErrorModel()
        {
        }
    }
}
