﻿using System;

using System.ComponentModel;
using PizzaExercise.ViewModels;
using Xamarin.Forms;

namespace PizzaExercise
{
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            (BindingContext as MainPageViewModel).Start();
        }
    }
}
