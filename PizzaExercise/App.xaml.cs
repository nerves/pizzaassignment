﻿using System;
using BackendlessAPI;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PizzaExercise
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            Backendless.InitApp("BB06653B-4C8B-B7B2-FF32-B59686641800", "C4300986-40CE-40EF-A37A-D4C0BBE6934A");

            MainPage = new MainPage();
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
